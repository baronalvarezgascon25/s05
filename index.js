// Mathematical Operations (-,*,/,%)
// Subtraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);
console.log(num3-num4);
console.log(numString1-num2);
console.log(numString2-num2);
console.log(numString1-numString2);

let sample2 = "Juan Dela Cruz";
console.log(sample2-numString1);

console.log(num1*num2);
console.log(numString1*num1);
console.log(numString1*numString2);
console.log(sample2*5);

let product1 = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;

console.log(product1);

console.log(product1/num2);
console.log(product2/5);
console.log(numString2/numString1);
console.log(numString2%numString1);

console.log(product2*0);
console.log(product3/0);

console.log(product2%num2);

console.log(product3%product2);
console.log(num1%num2);
console.log(num1%num1);

let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is he the MVP? " + isMVP);
console.log('Is he the current Admin? ' + isAdmin);

let array1 = ["Goku", "Picolo", "Gohan", "Vegeta"];
console.log(array1);
let array2 = ["One punch man", true, 500, "Saitama"];
console.log(array2);
let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63913902920", "8123 4567"],
	address: {
		houseNumber: '345',
		street: 'Diamond',
		city: 'Manila'
	}
};

console.log(person);
//Create a variable with a group of data: names from your favorite band
//Create a variable which contain multiple values of differing types and describes a single person: should contain multiple key value pairs. firstName, lastName isDeveloper, hasPortfolio, age, contact (2), address ( houseNumber, street, )

let idols = {
	names: ["Nagi Yanagi","Cassie Wei"]
};
let me = {
	firstName: "Baron",
	lastName: "Gascon",
	isDeveloper: false,
	hasPortfolio: false,
	age: 20,
	contact: ["+639397262320","+639494563322"],
	address: {
		houseNumber: 20,
		street: "St. Benedict",
		city: "Dasmarinas"
	}
};
console.log(idols);
console.log(me);

let sampleNull = null;

console.log(sampleNull);

let person2 = {
	name: "peter",
	age: 35
}
console.log(person2.isAdmin);

function printName(){
	console.log("My name is Jin");
};

printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();

function showSum(){
	console.log(25+6);
};

showSum();

function printName(name){
	console.log(`My name is ${name}`);
};

printName("V");

function displayMessage(language){
	console.log(`${language} is Fun`);
};
displayMessage("JavaScript");
displayMessage("C++");

function displayFullName(firstName, mi, lastName, age){
	console.log(`${mi} ${lastName} ${age}`)
};
displayFullName("Seok-Jin", "P", "Kim", 29);

function createFullName(firstName, middleName, lastName){
	console.log("I will no longer run because the function's value/result has been returned.");
	return `${firstName} ${middleName} ${lastName}`
	
};

let fullName1 = createFullName("Junkooook", "BTS", "Jen");

console.log(fullName1);